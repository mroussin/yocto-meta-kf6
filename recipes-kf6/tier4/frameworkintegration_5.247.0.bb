# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/frameworkintegration-5.247.0.tar.xz"
SRC_URI[sha256sum] = "e87dbc2a624360c3611b0a503790fe33e5d05cff44f25acdf8a7102aaaeb8615"

