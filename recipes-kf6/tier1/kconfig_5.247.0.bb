# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kconfig-5.247.0.tar.xz"
SRC_URI[sha256sum] = "ddc8e845b601f50a2d31e7a2fbff068a96cab59171b12905941f000221da7b0a"

