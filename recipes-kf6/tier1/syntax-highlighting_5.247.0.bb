# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/syntax-highlighting-5.247.0.tar.xz"
SRC_URI[sha256sum] = "b3a2c46daf3b6435209e0cffbe46604f2899bb870606fbc532c8c2959e525a7c"

