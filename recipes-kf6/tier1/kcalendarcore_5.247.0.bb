# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcalendarcore-5.247.0.tar.xz"
SRC_URI[sha256sum] = "027b13d7ebd2389a717479b3d0a6ff0eb76fc30239ef25424c593ddb5a7c7f85"

