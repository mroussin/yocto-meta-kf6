# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kuserfeedback-5.247.0.tar.xz"
SRC_URI[sha256sum] = "97eeb33392187f1bf7a655eefc5bb2c5af2976c8000d9b8684d728e405b91f4a"

