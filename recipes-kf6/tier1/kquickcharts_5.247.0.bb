# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kquickcharts-5.247.0.tar.xz"
SRC_URI[sha256sum] = "e5ae80db1a96fd416805f8fbb47c6150cfb4011d6229c0552684058c53f7df46"

