# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kitemmodels-5.247.0.tar.xz"
SRC_URI[sha256sum] = "8264ec5ce6b5aa8867a40d27db4a99af1d86f1298e38264ce4130ee382d54690"

