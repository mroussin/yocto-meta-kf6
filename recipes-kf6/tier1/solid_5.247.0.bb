# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/solid-5.247.0.tar.xz"
SRC_URI[sha256sum] = "8e95ad9900259a6952c4626f19b2b879310024b61847abdbe7f663de7730ee4f"

