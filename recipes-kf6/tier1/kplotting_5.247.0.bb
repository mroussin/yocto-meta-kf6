# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kplotting-5.247.0.tar.xz"
SRC_URI[sha256sum] = "8f84fd8b27fb82618ce26541a1910f766594e3b7fca134be82b12c0ff1f42b84"

