# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

SUMMARY = "KUserFeedback"
DESCRIPTION = "Framework for collecting user feedback for apps via telemetry and surveys"
HOMEPAGE = "https://invent.kde.org/libraries/kuserfeedback"
