# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/modemmanager-qt-5.247.0.tar.xz"
SRC_URI[sha256sum] = "5f7500a0754d918a8b51afba042c4b14260be6e02ae06832f23b5286a7429aba"

