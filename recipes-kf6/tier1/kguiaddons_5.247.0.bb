# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kguiaddons-5.247.0.tar.xz"
SRC_URI[sha256sum] = "ba035a69d642297e6aebd826c631680a3c588e8d2744d3629b5134c30ff58835"

