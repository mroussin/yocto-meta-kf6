# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/networkmanager-qt-5.247.0.tar.xz"
SRC_URI[sha256sum] = "3b9432e8b17114872f695c6973def587eb63aaf6e8270274842c5cf8a56b7956"

