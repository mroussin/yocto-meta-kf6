# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kdbusaddons-5.247.0.tar.xz"
SRC_URI[sha256sum] = "9e13871594d5951ced2ba88ea6c1b0f2f43ce4a94afb7ea84cbb0569178e6720"

