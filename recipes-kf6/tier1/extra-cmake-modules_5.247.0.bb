# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/extra-cmake-modules-5.247.0.tar.xz"
SRC_URI[sha256sum] = "9f3fa7e4e88ecc06f202698d5378bdf10c413d8a8d77b384e4a099d717754d65"

