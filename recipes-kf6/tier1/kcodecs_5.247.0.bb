# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcodecs-5.247.0.tar.xz"
SRC_URI[sha256sum] = "5f69d5d7bb7c883e8b88d121f243ff38d1e601a5d1e597ed0dcf61a7a244ec6b"

