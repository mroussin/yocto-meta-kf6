# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kwidgetsaddons-5.247.0.tar.xz"
SRC_URI[sha256sum] = "3bf42feac70e1cc07033a12e33248723d9fd1a0975596c88372b893ff7daa701"

