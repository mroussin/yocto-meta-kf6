# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kimageformats-5.247.0.tar.xz"
SRC_URI[sha256sum] = "73c3326e50c8b5c4a31fe0dd46e1f74139cf4604cac3e6277a985c58d6ba23c4"

