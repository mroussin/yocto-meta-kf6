# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/breeze-icons-5.247.0.tar.xz"
SRC_URI[sha256sum] = "d1a27bb2f85c39d2c50ca688a852b21480a95a68317d61d1ff627262ec7c0e11"

