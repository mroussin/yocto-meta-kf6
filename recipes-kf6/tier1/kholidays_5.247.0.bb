# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kholidays-5.247.0.tar.xz"
SRC_URI[sha256sum] = "d8837c7eb6a6a25ca367c2ecd24971f1beaf718a60cbbab2d2bf0be5f2ca360a"

