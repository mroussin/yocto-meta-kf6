# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/attica-5.247.0.tar.xz"
SRC_URI[sha256sum] = "884f4927fd1db74770b9768a79da0399cb98b0fe9ee3999598cb9fc36f845b97"

