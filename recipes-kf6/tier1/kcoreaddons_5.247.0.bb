# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcoreaddons-5.247.0.tar.xz"
SRC_URI[sha256sum] = "795b73ac664d1ed5fe202a062674f86ec6fd9a64108c74814c0d64212f5b5701"

