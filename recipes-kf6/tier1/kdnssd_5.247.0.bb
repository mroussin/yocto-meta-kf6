# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kdnssd-5.247.0.tar.xz"
SRC_URI[sha256sum] = "76408bf094306ffd5caa6307b8d46ab28c0603847e6fa3f3aca62b1085ce2d8d"

