# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kirigami-5.247.0.tar.xz"
SRC_URI[sha256sum] = "1f71abdcab2108b699881060cb723d8d9934c2e98b264901cccbf41d8e42b7d5"

