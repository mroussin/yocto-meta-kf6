# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/prison-5.247.0.tar.xz"
SRC_URI[sha256sum] = "7f6d3ebd0a033bf3d483d574be7de7086eeb7e588ae927f9783668036cc678b7"

