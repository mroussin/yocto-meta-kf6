# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kwindowsystem-5.247.0.tar.xz"
SRC_URI[sha256sum] = "23c7654dbcd0590c508d41d8fb56e7d812852ad2404f7229f787e271abfef99b"

