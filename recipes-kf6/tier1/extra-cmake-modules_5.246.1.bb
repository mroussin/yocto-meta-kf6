# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.246.0/extra-cmake-modules-5.246.1.tar.xz"
SRC_URI[sha256sum] = "917b4a9d0c431984acdca94422dc8ffc0d56bd9ebee591da82633fc38d9fea54"

