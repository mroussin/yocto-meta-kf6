# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/ki18n-5.247.0.tar.xz"
SRC_URI[sha256sum] = "52ec5d7e09bd61cfd722aa9561940a213c3b7f44bf61d73966d057d206b6f40d"

