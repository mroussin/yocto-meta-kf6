# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/karchive-5.247.0.tar.xz"
SRC_URI[sha256sum] = "50e4f901f276a12b4442e68ccdb3dd192d0e35a2085cb9decd37fb4b837bc97d"

