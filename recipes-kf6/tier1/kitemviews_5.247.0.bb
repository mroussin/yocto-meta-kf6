# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kitemviews-5.247.0.tar.xz"
SRC_URI[sha256sum] = "2348f80dad6eeefece0207d992ad591534b0fd6b3f5cb5153d1f3a48df5d4af6"

