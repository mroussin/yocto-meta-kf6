# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/threadweaver-5.247.0.tar.xz"
SRC_URI[sha256sum] = "e1ce74beb64a3fcd05bd527e84ceb3910d5e45852bf1f6f49c7138b9e737f1a7"

