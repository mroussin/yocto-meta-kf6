# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/bluez-qt-5.247.0.tar.xz"
SRC_URI[sha256sum] = "7ff93bbc8aa2295fdfa2c8d664288a6884f3c7e05901f1469a9cedc8f518f722"

