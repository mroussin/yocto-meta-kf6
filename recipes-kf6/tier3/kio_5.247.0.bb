# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kio-5.247.0.tar.xz"
SRC_URI[sha256sum] = "6aad5b2220c39b7314dead8814196dcf0515d73d435c713f3b268fc2f2e949e7"

