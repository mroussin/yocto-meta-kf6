# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kservice-5.247.0.tar.xz"
SRC_URI[sha256sum] = "fe336f7f2ee3e945afacdcaeb619bb8ba2796780ccb3c944e2c2103c1d613bc1"

