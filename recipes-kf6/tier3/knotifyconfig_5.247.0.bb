# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/knotifyconfig-5.247.0.tar.xz"
SRC_URI[sha256sum] = "4ad788397f926fca627cb98825cef510e49a2740f18c99b37fe042ce8bf822aa"

