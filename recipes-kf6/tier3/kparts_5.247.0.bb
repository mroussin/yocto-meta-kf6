# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kparts-5.247.0.tar.xz"
SRC_URI[sha256sum] = "ea6d7e20b282d3f25a5be379a0fab2cf058a515f169c7270479c807413932aa4"

