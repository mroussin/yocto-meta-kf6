# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/ktexteditor-5.247.0.tar.xz"
SRC_URI[sha256sum] = "218238c1bd01c6403fd35a12b513a7a5fc91bc8b3e32785e35faab6211191911"

