# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kded-5.247.0.tar.xz"
SRC_URI[sha256sum] = "b653417ca80891ebab342c29dee62e5d6e33cf1f0ccf8801fda0b8e58b1b038f"

