# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/purpose-5.247.0.tar.xz"
SRC_URI[sha256sum] = "8703ce59d6157d3760658e493b3fa95ece52ce843e392977b79283298bc18644"

