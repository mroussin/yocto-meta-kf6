# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/qqc2-desktop-style-5.247.0.tar.xz"
SRC_URI[sha256sum] = "1065115596d29a4c36108c5c4976e398e7a7e48d05dec4d98a8f0ec4a02a3dc8"

