# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kpeople-5.247.0.tar.xz"
SRC_URI[sha256sum] = "f427f47531c5700825a428335ad962f6a709617b2a802deff081a45a4abec50c"

