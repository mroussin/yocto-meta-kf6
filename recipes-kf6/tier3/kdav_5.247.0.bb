# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kdav-5.247.0.tar.xz"
SRC_URI[sha256sum] = "b418e06d25279f31833deb85764f32c6180e411fb9c56bd81c83b06614b225ed"

