# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/baloo-5.247.0.tar.xz"
SRC_URI[sha256sum] = "2136cbabac60627f50b6616c9849cf5df4ffd627d99b796e890f1a69048fc54d"

