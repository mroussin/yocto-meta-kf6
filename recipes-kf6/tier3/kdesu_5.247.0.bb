# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kdesu-5.247.0.tar.xz"
SRC_URI[sha256sum] = "bbadcfc3c632dc051a7b714b33126e80236cc674fa3fddadee967da918074be3"

