# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kdeclarative-5.247.0.tar.xz"
SRC_URI[sha256sum] = "a6a4c10e4af611809eb3b5f89c3499677a33481c5b5264e1003698ba9b97df01"

