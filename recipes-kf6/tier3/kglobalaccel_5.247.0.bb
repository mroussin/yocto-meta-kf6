# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kglobalaccel-5.247.0.tar.xz"
SRC_URI[sha256sum] = "41668826f2f4b6796672d3f291cf75252db88af65a1b2bc62fe95fdd59d0405e"

