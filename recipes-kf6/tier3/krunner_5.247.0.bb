# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/krunner-5.247.0.tar.xz"
SRC_URI[sha256sum] = "9a1ddbcc3803fc07cc422c9710a9a7d133af50f94a15b643dd744fd3e50a50c1"

