# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/ktextwidgets-5.247.0.tar.xz"
SRC_URI[sha256sum] = "da82f555f1af642e7b3f8468acb7a4019934bee8a2e672c2b91d8c56276faba7"

