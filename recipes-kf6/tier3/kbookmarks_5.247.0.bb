# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kbookmarks-5.247.0.tar.xz"
SRC_URI[sha256sum] = "c3409c496090fc028a05f9054113606a3495202cd43b0384ff44618f8f578000"

