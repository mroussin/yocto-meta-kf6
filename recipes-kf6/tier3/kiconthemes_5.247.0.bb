# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kiconthemes-5.247.0.tar.xz"
SRC_URI[sha256sum] = "b8f8dd4ab3af70d063845560fad12132c9b283c575ede71c944f8b1a96c4036c"

