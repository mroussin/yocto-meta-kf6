# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/knewstuff-5.247.0.tar.xz"
SRC_URI[sha256sum] = "d6d2dbacade38c8e762bf5d6337cb7dd2c11479eaa028f751584e8efc98da570"

