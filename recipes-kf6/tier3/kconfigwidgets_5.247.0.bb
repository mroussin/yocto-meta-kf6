# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kconfigwidgets-5.247.0.tar.xz"
SRC_URI[sha256sum] = "44b07f66519e44d37d66a1cefdafd6748885b7ec79d596cbc46afc56a1a3d7ed"

