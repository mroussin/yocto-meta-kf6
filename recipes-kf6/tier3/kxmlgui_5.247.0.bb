# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kxmlgui-5.247.0.tar.xz"
SRC_URI[sha256sum] = "2dc6be0261c0627e8a7a0b61789bb6a8c8e85435e11b602a12b8bd10ae720ea2"

