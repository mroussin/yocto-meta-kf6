# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcmutils-5.247.0.tar.xz"
SRC_URI[sha256sum] = "148adef410fe1754eb12b70ef5e370d2debb09355cf3a70461783babf63ccbe7"

