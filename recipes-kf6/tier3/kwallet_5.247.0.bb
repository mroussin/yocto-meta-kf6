# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kwallet-5.247.0.tar.xz"
SRC_URI[sha256sum] = "68f44a6a66c371f1854ee15fb2d85b54cf0d032b11f61e6db40df482cc112871"

