# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

SUMMARY = "Classes to read and interact with KColorScheme"
HOMEPAGE = "https://api.kde.org/frameworks/kcolorscheme/html/index.html"
