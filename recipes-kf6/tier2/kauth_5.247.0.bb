# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kauth-5.247.0.tar.xz"
SRC_URI[sha256sum] = "222076b307f0a34d1f42b97df8398584fa456cd1962292dbf797b144d4a0e43c"

