# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcompletion-5.247.0.tar.xz"
SRC_URI[sha256sum] = "0aa0496c3cef564d3d610ca7f13347f9f0ef80b71100ac4fb2fc03ea3e04d0a4"

