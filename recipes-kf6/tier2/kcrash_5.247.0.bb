# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcrash-5.247.0.tar.xz"
SRC_URI[sha256sum] = "b3b73004bc4c32e0f79fe567727c88984c4405be4193d714db83d1cad4eff02a"

