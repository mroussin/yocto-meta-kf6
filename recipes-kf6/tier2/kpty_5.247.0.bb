# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kpty-5.247.0.tar.xz"
SRC_URI[sha256sum] = "62c905e756a6b88e42b2f0088728f55de5443bdcc02368db7d3c68d8417f54a8"

