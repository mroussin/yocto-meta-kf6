# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kunitconversion-5.247.0.tar.xz"
SRC_URI[sha256sum] = "176a77bab0675d8673cf00e4c3536206dc73708f2e1661c95c6bef908e0746e3"

