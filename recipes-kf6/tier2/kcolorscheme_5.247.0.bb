# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcolorscheme-5.247.0.tar.xz"
SRC_URI[sha256sum] = "ce4afae71380ca76ec232e5c6c50f449e16af46e43024b68509873b1178972c3"

