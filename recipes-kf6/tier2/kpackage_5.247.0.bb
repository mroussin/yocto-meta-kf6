# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kpackage-5.247.0.tar.xz"
SRC_URI[sha256sum] = "e6e4c8acc9c64569c1ca6e6d8ceba6de730e17c4e7100bc1d756453ac0b61574"

