# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kstatusnotifieritem-5.247.0.tar.xz"
SRC_URI[sha256sum] = "965c832cbec5fb0523b0d32af013a7dcc5d456f2551f7fa3f8beb6bb40dbae25"

