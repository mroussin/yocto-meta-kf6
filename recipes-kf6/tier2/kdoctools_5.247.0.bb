# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kdoctools-5.247.0.tar.xz"
SRC_URI[sha256sum] = "51434d1348c389856c29bca3c530806544fd679c27200187ad2ce4eb726cf2a7"

