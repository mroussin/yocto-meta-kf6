# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kjobwidgets-5.247.0.tar.xz"
SRC_URI[sha256sum] = "0e39b465e6073757fb92ebdfc68b400cc2b37f3e28c343997d8fe2528cfb2a59"

