# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/ksvg-5.247.0.tar.xz"
SRC_URI[sha256sum] = "7009a82fcfe1c43b62187f9ec81b8d1f39da507eb329fc96d143a7748dece64a"

