# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kfilemetadata-5.247.0.tar.xz"
SRC_URI[sha256sum] = "70b470d97885f609443d04f5a0c4dd357f806487b22295cceee989fc1d72489f"

