# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/kcontacts-5.247.0.tar.xz"
SRC_URI[sha256sum] = "424963ae9501115a5e4172d9f9433f7824745d732a6760560aa4b93ed7aea176"

