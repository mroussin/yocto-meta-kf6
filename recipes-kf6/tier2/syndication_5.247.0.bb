# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/syndication-5.247.0.tar.xz"
SRC_URI[sha256sum] = "9618a44ac786d95f3044d06d83177f2f6f51b44182de6cb85281014e9b359f66"

