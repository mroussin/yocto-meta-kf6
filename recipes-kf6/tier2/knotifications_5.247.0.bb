# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/unstable/frameworks/5.247.0/knotifications-5.247.0.tar.xz"
SRC_URI[sha256sum] = "b5afc48becfe6778219613e41c4350b8f23ac6376f6d5f8ac5616a6702f9fe7a"

